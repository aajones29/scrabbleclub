package scrabbleclub.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import scrabbleclub.models.MemberRequest;
import scrabbleclub.models.entities.MemberDetails;
import scrabbleclub.respository.MemberRepository;

/**
 *	Controller for displaying the list of members
 */
@Controller
public class MemberListController {
	
	@Autowired
	private MemberRepository memberRespository;

	@GetMapping("/")
	public String listMembers(Model model) {
		List<MemberDetails> memberList = memberRespository.findAll();
		model.addAttribute("memberlist", memberList);
		model.addAttribute("memberRequest", new MemberRequest());
		return "index";
	}
	
}
