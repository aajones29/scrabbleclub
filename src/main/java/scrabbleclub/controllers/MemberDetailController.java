package scrabbleclub.controllers;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import scrabbleclub.models.MemberRequest;
import scrabbleclub.models.TempScoreTracker;
import scrabbleclub.models.entities.MemberDetails;
import scrabbleclub.models.entities.Scores;
import scrabbleclub.models.viewobjects.Highscore;
import scrabbleclub.models.viewobjects.MemberProfile;
import scrabbleclub.respository.MemberRepository;
import scrabbleclub.respository.ScoresRepository;

/**
 * Controller class to operate the retrieval and passage of members details
 */
@Controller
public class MemberDetailController {

	private static final String LOSS = "loss";
	private static final String WIN = "win";

	@Autowired
	private MemberRepository memberRespository;
	
	@Autowired
	private ScoresRepository scoresRepository;

	/**
	 * Endpoint for displaying a specific members details by name.
	 * @param memberRequest
	 * @param model
	 * @return
	 */
	@PostMapping("/memberdetail")
	public String listMembers(@ModelAttribute("memberRequest")MemberRequest memberRequest, Model model) {
		MemberDetails memberDetail = memberRespository.findMemberDetailsByName(memberRequest.getName());
		
		List<Scores> scores = scoresRepository.findScoresByName(memberRequest.getName());
		Scores bestScore = Collections.max(scores, Comparator.comparing(s -> s.getScore()));
		TempScoreTracker tempScoreTracker = getScoreInformation(scores);
		
		MemberProfile profile = new MemberProfile();
		profile.setName(memberDetail.getName());
		profile.setHighScore(setHighscore(bestScore));
		profile.setAveragescore(tempScoreTracker.getAverageScore());
		profile.setLosscount(tempScoreTracker.getLossCount());
		profile.setWincount(tempScoreTracker.getWinCount());
		
		
		model.addAttribute("memberprofile", profile);
		return "memberprofile";
	}

	/**
	 * Calculates score information based off the list passed in
	 * @param scores
	 * @return score information
	 */
	private TempScoreTracker getScoreInformation(List<Scores> scores) {
		int winCount = 0;
		int lossCount = 0;
		int totalScore = 0;
		for (Scores score :scores) {
			totalScore += score.getScore();
			if (WIN.equalsIgnoreCase(score.getResult())) {
				winCount++;	
			} else if (LOSS.equalsIgnoreCase(score.getResult())) {
				lossCount++;
			}
		}
		
		TempScoreTracker scoreTracker = new TempScoreTracker();
		scoreTracker.setAverageScore(totalScore/scores.size());
		scoreTracker.setWinCount(winCount);
		scoreTracker.setLossCount(lossCount);
		return scoreTracker;
	}

	/**
	 * Calculates and creates a highscore object based off the list of scores passed in
	 * @param bestScore
	 * @return Highscore object
	 */
	private Highscore setHighscore(Scores bestScore) {
		Highscore highscore = new Highscore();
		highscore.setHighscore(bestScore.getScore());
		highscore.setOpponent(bestScore.getOpponent());
		highscore.setWhenScored(bestScore.getTimeScored());
		return highscore;
	}
	
}
