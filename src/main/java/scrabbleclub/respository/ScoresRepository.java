package scrabbleclub.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import scrabbleclub.models.entities.Scores;

@Repository
public interface ScoresRepository extends JpaRepository<Scores, Long> {

	public List<Scores> findScoresByName(final String name);
}
