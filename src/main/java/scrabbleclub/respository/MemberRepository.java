package scrabbleclub.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import scrabbleclub.models.entities.MemberDetails;

@Repository
public interface MemberRepository extends JpaRepository<MemberDetails, Long>{
	
	public MemberDetails findMemberDetailsByName(final String name);

}
