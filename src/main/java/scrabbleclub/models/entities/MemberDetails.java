package scrabbleclub.models.entities;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Object to represent the Memberdetails table in the H2 database
 */
@Entity
@Table(name ="memberdetails")
public class MemberDetails {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private String name;
	@Column
	private String contactNumber;
	@Column
	private BigInteger matchCount;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public BigInteger getMatchCount() {
		return matchCount;
	}
	public void setMatchCount(BigInteger matchCount) {
		this.matchCount = matchCount;
	}
	
}
