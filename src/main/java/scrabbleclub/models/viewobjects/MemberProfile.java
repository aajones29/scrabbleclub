package scrabbleclub.models.viewobjects;

public class MemberProfile {
	
	private String name;
	private int wincount;
	private int losscount;
	private int averagescore;
	public Highscore highscore;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getWincount() {
		return wincount;
	}
	public void setWincount(int wincount) {
		this.wincount = wincount;
	}
	public int getLosscount() {
		return losscount;
	}
	public void setLosscount(int losscount) {
		this.losscount = losscount;
	}
	public int getAveragescore() {
		return averagescore;
	}
	public void setAveragescore(int averagescore) {
		this.averagescore = averagescore;
	}
	public Highscore getHighScore() {
		return highscore;
	}
	public void setHighScore(Highscore highScore) {
		this.highscore = highScore;
	}
	

}
