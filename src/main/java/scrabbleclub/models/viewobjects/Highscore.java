package scrabbleclub.models.viewobjects;

public class Highscore {
	
	private int highscore;
	private String whenScored;
	private String opponent;
	
	public int getHighscore() {
		return highscore;
	}
	public void setHighscore(int highscore) {
		this.highscore = highscore;
	}
	public String getWhenScored() {
		return whenScored;
	}
	public void setWhenScored(String whenScored) {
		this.whenScored = whenScored;
	}
	public String getOpponent() {
		return opponent;
	}
	public void setOpponent(String opponent) {
		this.opponent = opponent;
	}

}
