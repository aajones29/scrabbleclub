package scrabbleclub.models;

public class TempScoreTracker {
	
	private int winCount;
	private int lossCount;
	private int AverageScore;
	
	public int getWinCount() {
		return winCount;
	}
	public void setWinCount(int winCount) {
		this.winCount = winCount;
	}
	public int getLossCount() {
		return lossCount;
	}
	public void setLossCount(int lossCount) {
		this.lossCount = lossCount;
	}
	public int getAverageScore() {
		return AverageScore;
	}
	public void setAverageScore(int averageScore) {
		AverageScore = averageScore;
	}
	
	

}
