insert into memberdetails values (1, '0123456', 10, 'Edith');
insert into memberdetails values (2, '9878978', 12, 'Geraldine');
insert into memberdetails values (3, '8888877', 8, 'Cliff');
insert into memberdetails values (4, '0000222', 10, 'Albert');
insert into memberdetails values (5, '0178965', 10, 'Harold');

insert into scores values (1, 'Edith', 'Geraldine', 'win', 155, '20-01-18 13:01:55');
insert into scores values (2, 'Edith', 'Cliff', 'lose', 222, '10-03-18 15:05:55');
insert into scores values (3, 'Albert', 'Geraldine', 'win', 554, '25-06-18 22:06:53');
insert into scores values (4, 'Edith', 'Albert', 'win', 123, '16-07-18 08:21:55');
insert into scores values (5, 'Albert', 'Geraldine', 'lose', 22, '11-09-18 09:21:55');
insert into scores values (6, 'Edith', 'Albert', 'lose', 165, '26-10-18 17:21:55');
insert into scores values (7, 'Harold', 'Cliff', 'win', 866, '28-10-18 15:21:55');
insert into scores values (8, 'Geraldine', 'Albert', 'win', 543, '04-11-18 16:41:53');
insert into scores values (9, 'Geraldine', 'Harold', 'win', 275, '11-12-18 17:31:45');
insert into scores values (10, 'Geraldine', 'Cliff', 'lose', 344, '19-12-18 21:16:33');
insert into scores values (11, 'Cliff', 'Albert', 'lose', 125, '10-01-19 19:41:22');
insert into scores values (12, 'Cliff', 'Geraldine', 'win', 433, '25-01-19 13:01:44');
insert into scores values (13, 'Edith', 'Cliff', 'lose', 121, '12-02-19 13:51:55');
